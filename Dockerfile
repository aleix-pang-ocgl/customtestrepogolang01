FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0

WORKDIR /src/
RUN echo "Hello World!!333"

# Copy and download the dependencies
COPY go.mod go.sum /src/
RUN go mod download

# Build the application
COPY . /src/
RUN go build -o /bin/main

# Execute
FROM scratch AS exe
COPY --from=build /bin/main /bin/main
ENTRYPOINT ["/bin/main"]

# Run test case
# FROM build AS test
# RUN go get -u github.com/jstemmer/go-junit-report
# RUN go test -v 2>&1 | go-junit-report > /bin/report.xml
# RUN go test -v 2>&1 | go-junit-report > report.xml
# RUN echo "Hello World!!333"
# RUN file="$(ls)" && echo $file

# # test output
# FROM scratch as testoutput
# COPY --from=test /bin/report.xml report2.xml
# COPY --from=test /src/report.xml report.xml