package main

import "fmt"

func Hello() string {
	// test pr2 - changes
	return "Hello, world"
}

func main() {
	// test PR1
	fmt.Println(Hello())
}
